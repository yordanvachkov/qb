package com.biranful.qb.v2;


import com.biranful.qb.v2.utils.QueryBuilder;
import com.biranful.qb.v2.utils.QueryInfo;
import com.biranful.qb.v2.utils.Select;
import com.biranful.qb.v2.utils.WhereOperator;

import java.util.List;

public class SqlUtil {

    public QueryInfo projection(String tableName, List<String> columns) {
        Select select = QueryBuilder.builder()
                .select();

        if (columns == null || columns.isEmpty())
            select.all();
        else
            select.column(columns.toArray(new String[0]));

        return select.from()
                .table(tableName)
                .build();
    }

    public QueryInfo selection(String tableName, String column, String operator) {
        return QueryBuilder.builder()
                .select()
                .all()
                .from()
                .table(tableName)
                .where()
                .column(column, WhereOperator.valueOf(operator))
                .build();
    }

    public QueryInfo insert(String tableName, List<String> columns) {
        return QueryBuilder.builder()
                .insert()
                .intoTable(tableName)
                .columns(columns)
                .build();
    }
}

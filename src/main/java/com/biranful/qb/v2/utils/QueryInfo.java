package com.biranful.qb.v2.utils;

public class QueryInfo {
    private final StringBuilder sql;

    QueryInfo() {
        this.sql = new StringBuilder();
    }

    public QueryInfo(QueryInfo clone) {
        this.sql = clone.sql;
    }

    QueryInfo append(String expression) {
        sql.append(expression);
        return this;
    }

    QueryInfo appendLine(String expression) {
        sql.append(expression)
                .append("\n");
        return this;
    }

    public String getQuery() {
        return sql.toString();
    }
}

package com.biranful.qb.v2.utils;

public class From implements Query {
    private QueryInfo queryInfo;

    From(QueryInfo queryInfo) {
        this.queryInfo = queryInfo;
        this.queryInfo.append("FROM ");
    }

    public From table(String table) {
        queryInfo.appendLine(table + " ");
        return this;
    }

    public Join join(JoinType joinType) {
        return new Join(queryInfo, joinType);
    }

    public Where where() {
        return new Where(queryInfo);
    }

    @Override
    public QueryInfo build() {
        return queryInfo;
    }
}

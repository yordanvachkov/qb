package com.biranful.qb.v2.utils;

public interface Query {
    QueryInfo build();
}

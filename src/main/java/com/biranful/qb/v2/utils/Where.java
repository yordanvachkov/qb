package com.biranful.qb.v2.utils;

public class Where implements Query {

    private QueryInfo queryInfo;

    Where(QueryInfo queryInfo) {
        this.queryInfo = queryInfo;
        this.queryInfo.append("WHERE ");
    }

    public Where column(String column, WhereOperator operator) {
        String condition = column + " " +
                operator.getValue() + " ?";
        queryInfo.append(condition);
        return this;
    }

    public Where and() {
        queryInfo.append(" AND ");
        return this;
    }

    public Where or() {
        queryInfo.append(" OR ");
        return this;
    }

    @Override
    public QueryInfo build() {
        return queryInfo;
    }
}

package com.biranful.qb;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class ReflectionMain {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        TestClass testClass = new TestClass();

        Class<TestClass> testClassClass = TestClass.class;

//        String name = testClassClass.getName();
//        String simpleName = testClassClass.getSimpleName();
//
//        System.out.println(name);
//        System.out.println(simpleName);
//
//        Field[] declaredFields = testClassClass.getDeclaredFields();
//        Method[] declaredMethods = testClassClass.getDeclaredMethods();
//////        TypeVariable<Class<TestClass>>[] typeParameters = testClassClass.getTypeParameters();
//////        TypeVariable<Class<TestClass>> typeParameter = typeParameters[0];
//
//        Arrays.stream(declaredFields)
//                .forEach(field -> System.out.println(field.getName()));
//
//        Arrays.stream(declaredMethods)
//                .forEach(method -> System.out.println(method.getName()));

//        Field somePrivateField = testClassClass.getDeclaredField("somePrivateField");
//        somePrivateField.setAccessible(true);
//        System.out.println(somePrivateField.get(testClass));
//        somePrivateField.set(testClass, 1);
//        System.out.println(somePrivateField.get(testClass));

        Method sum = testClassClass.getDeclaredMethod("sum", int.class);
        Object result = sum.invoke(testClass, 1);
        System.out.println(result);
    }

    public static class TestClass {
        private static final int MAIN_VARIABLE = 5;

        private int somePrivateField;

        public int sum(int b) {
            return MAIN_VARIABLE + b;
        }
    }
}

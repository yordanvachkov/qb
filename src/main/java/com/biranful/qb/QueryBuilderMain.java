package com.biranful.qb;

import com.biranful.qb.entity.CreditCard;
import com.biranful.qb.v1.QueryBuilderImpl;
import com.biranful.qb.v2.SqlUtil;

import java.util.Arrays;

public class QueryBuilderMain {
    public static void main(String[] args) {
        QueryBuilderImpl<CreditCard> queryBuilder = new QueryBuilderImpl<>(CreditCard.class);
        SqlUtil sqlUtil = new SqlUtil();

        //projection
        System.out.println("----------");
        String v1 = queryBuilder.select().getQuery();
        System.out.println(v1);

        System.out.println();
        String v2 = sqlUtil.projection("creditCard", null).getQuery();

        System.out.println(v2);
        System.out.println("----------");

//        //selection
        System.out.println("----------");
        v1 = queryBuilder.select(new String[]{"cardNumber"}).getQuery();
        System.out.println(v1);
        System.out.println();
        v2 = sqlUtil.selection("creditCard", "cardNumber", "EQ").getQuery();
        System.out.println(v2);
        System.out.println("----------");

        //insert
        System.out.println("----------");
        v1 = queryBuilder.insert().getQuery();
        System.out.println(v1);
        System.out.println();
        v2 = sqlUtil.insert("creditCard", Arrays.asList(new String[]{"holderName", "cardNumber", "cvv", "expiry"})).getQuery();
        System.out.println(v2);
        System.out.println("----------");
    }
}


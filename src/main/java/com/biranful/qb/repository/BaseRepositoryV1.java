package com.biranful.qb.repository;

import com.biranful.qb.ReflectionUtil;
import com.biranful.qb.entity.Base;
import com.biranful.qb.v1.QueryBuilder;
import com.biranful.qb.v1.QueryBuilderImpl;
import com.biranful.qb.v1.QueryInfo;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class BaseRepositoryV1<T extends Base> {
    private Class<T> tClass;
    private QueryBuilder queryBuilder;

    public BaseRepositoryV1(Class<T> tClass) {
        this.tClass = tClass;
        this.queryBuilder = new QueryBuilderImpl<>(tClass);
    }

    public Optional<T> get(String where, Object value) {
        QueryInfo select = queryBuilder.select(new String[]{where});

        // Database connectivity and execution of the query

        return Optional.empty();
    }

    public List<T> get() {
        List<T> result = new ArrayList<>();
        List<Field> fields = ReflectionUtil.getClassFields(tClass, new ArrayList<>());

        QueryInfo select = queryBuilder.select();

        // Database connectivity and execution of the query

        return Collections.emptyList();
    }

    public Optional<T> insert(T entity, String table) {
        List<Field> fields = ReflectionUtil.getClassFields(tClass, new ArrayList<>());
        QueryInfo insert = queryBuilder.insert();

        // Database connectivity and execution of the query

        return Optional.empty();
    }
}

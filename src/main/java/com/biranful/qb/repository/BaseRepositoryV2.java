package com.biranful.qb.repository;

import com.biranful.qb.ReflectionUtil;
import com.biranful.qb.entity.Base;
import com.biranful.qb.v2.SqlUtil;
import com.biranful.qb.v2.utils.From;
import com.biranful.qb.v2.utils.JoinType;
import com.biranful.qb.v2.utils.QueryBuilder;
import com.biranful.qb.v2.utils.QueryInfo;
import com.biranful.qb.v2.utils.WhereOperator;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class BaseRepositoryV2<T extends Base> {
    private static final String ID = "id";

    private Class<T> tClass;
    private SqlUtil sqlUtil;

    BaseRepositoryV2(Class<T> tClass) {
        this.tClass = tClass;
        sqlUtil = new SqlUtil();
    }


    public List<T> get() {
        QueryInfo projection = sqlUtil.projection(tClass.getSimpleName(), null);

        // Database connectivity and execution of the query

        return Collections.emptyList();
    }

    public Optional<T> get(Integer id) {
        String idColumn = tClass.getSimpleName() + "." + ID;
        QueryInfo selection = sqlUtil.selection(tClass.getSimpleName(), idColumn, "EQ");

        // Database connectivity and execution of the query

        return Optional.empty();
    }

    public Optional<T> insert(T entity) {
        Map<String, Object> data = dataToBeInsertedForEntity(entity);

        QueryInfo insert = sqlUtil.insert(tClass.getSimpleName(), new ArrayList<>(data.keySet()));

        // Database connectivity and execution of the query

        return Optional.empty();
    }

    private Map<String, Object> dataToBeInsertedForEntity(T entity) {
        Map<String, Object> data = new HashMap<>();

        List<Field> declaredFields = ReflectionUtil.getClassFields(tClass, new ArrayList<>());
        for (Field field : declaredFields) {

            field.setAccessible(true);

            try {
                String fieldName = field.getName();
                Object value = field.get(entity);

                if (!field.getType().isInstance(Base.class) && !ID.equals(fieldName)) {
                    data.put(nameConversion(fieldName), value);
                } else if (field.getType().isInstance(Base.class)) {
                    data.put(nameConversion(fieldName) + "_id", ((Base) value).getId());
                }
            } catch (IllegalAccessException e) {
                // ignore
            }
        }

        return data;
    }

    private String nameConversion(String fieldName) {
        char[] chars = fieldName.toCharArray();
        StringBuilder sb = new StringBuilder(0);
        sb.append(chars[0]);

        for (int i = 1; i < chars.length; i++) {
            String letter = Character.toString(chars[i]);
            if (letter.equals(letter.toUpperCase())) {
                sb.append('_');
            }

            sb.append(letter.toLowerCase());
        }

        return sb.toString();
    }
}

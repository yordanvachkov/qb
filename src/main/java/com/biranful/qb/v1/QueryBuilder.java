package com.biranful.qb.v1;

public interface QueryBuilder {
    QueryInfo select();

    QueryInfo select(String[] where);

    QueryInfo insert();
}

package com.biranful.qb.v1;

import java.util.Map;

public class QueryInfo {
    private String query;
    private Map<Integer, String> placeholders;

    public QueryInfo(String query, Map<Integer, String> placeholders) {
        this.query = query;
        this.placeholders = placeholders;
    }

    public String getQuery() {
        return query;
    }

    public Map<Integer, String> getPlaceholders() {
        return placeholders;
    }
}
